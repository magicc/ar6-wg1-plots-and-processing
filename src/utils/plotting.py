import numpy as np


SCENARIO_PALETTE = {
    k: np.array(v) / 256
    for k, v in {
        "ssp119": (0, 173, 207),
        "ssp126": (23, 60, 102),
        "ssp245": (247, 148, 32),
        "ssp370": (231, 29, 37),
        "ssp370-lowNTCF": (242, 17, 17),
        "ssp434": (34, 116, 175),
        "ssp460": (177, 114, 78),
        "ssp585": (149, 27, 30),
        "ssp534-over": (147, 57, 122),
        "rcp26": (0, 0, 255),
        "rcp45": (121, 188, 255),
        "rcp60": (196, 121, 0),
        "rcp85": (153, 0, 2),
    }.items()
}
SCENARIO_PALETTE["historical"] = "tab:gray"

RCM_PALETTE = {
    "MAGICCv7.5.1": "tab:orange",
    "OSCARv3.1.1": "tab:red",
    "CICERO-SCM": "tab:pink",
    "FaIRv1.6.2": "tab:cyan",
    "assessed range": "tab:blue",
}
