import os.path

import json
import pandas as pd
import pymagicc
import scmdata

from ._version import get_versions
__version__ = get_versions()['version']
del get_versions


DATA_DIR = os.path.join(os.path.dirname(__file__), "..", "..", "data")

MAGICC_VERSION = "v20210217"
MAGICC_ID = "0fd0f62-derived-metrics-id-f023edb-drawnset.json"
MAGICC_OUTPUT_DIR = os.path.join(
    DATA_DIR,
    "raw",
    "magicc",
    MAGICC_VERSION,
)

MAGICC_IDEALISED_OUTPUT_DIR = "output_210_idealised_experiments_2021_02_17_230201"
MAGICC_SSP_RCP_OUTPUT_DIR = "output_110_ssps_and_rcps_2021_02_17_230155"

OSCAR_VERSION = "v20210217"

CICERO_VERSION = "2.0.1"

FAIR_VERSION = "v20210211"

CH7_TWO_LAYER_VERSION = "v20210213"


ASSESSED_RANGES_FILE = os.path.join(
    DATA_DIR,
    "raw",
    "assessed-ranges",
    "v20210222-wg1-assessed-ranges.csv"
)

_ars = pd.read_csv(ASSESSED_RANGES_FILE)
gsat_assessment = _ars.loc[
    _ars["RCMIP name"] == "Surface Air Temperature Change World ssp245 1995-2014",
    :
]

# Reference period for historical GSAT assessment
TEMP_ADJUST_REF_PERIOD = range(
    int(gsat_assessment["norm_period_start"]), 
    int(gsat_assessment["norm_period_end"]) + 1
)

# Evaluation period for historical GSAT assessment
TEMP_ADJUST_PERIOD = range(
    int(gsat_assessment["evaluation_period_start"]), 
    int(gsat_assessment["evaluation_period_end"]) + 1
)

# Median warming for historical GSAT assessment (Box 2.3)
TEMP_ADJUST_TARGET = float(gsat_assessment["central"])


def add_magicc_id_metadata(inscmrun):
    inscmrun.metadata["magicc_run_id"] = MAGICC_SSP_RCP_OUTPUT_DIR
    inscmrun.metadata["magicc_drawnset"] = MAGICC_ID
    
    return inscmrun
