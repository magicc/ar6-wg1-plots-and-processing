import netcdf_scm.io
import scmdata


def get_longest_ensemble_member_for_climate_model(idf, years, years_no_nan):
    """
    Get longest ensemble ensemble member for each model for CMIP
    """
    db_cmip = []
    for sdf in idf.filter(year=years).groupby(["scenario", "climate_model"]):
        sdf_ts_no_nan = sdf.filter(year=years_no_nan).timeseries().dropna()
        if sdf_ts_no_nan.empty:
            print(
                "No useable data for {} {}".format(
                    sdf.get_unique_meta("climate_model", True),
                    sdf.get_unique_meta("scenario", True),
                )
            )
            continue

        sdf_ts = sdf.timeseries().loc[sdf_ts_no_nan.index, :]
        nan_times = sdf_ts.isnull().sum(axis="columns")
        longest_runs = scmdata.ScmRun(sdf_ts.loc[nan_times == nan_times.min(), :])

        member_ids = longest_runs["member_id"].unique()

        # get the shortest strings first then sort
        # this ensures that we choose e.g. r1i1ip1f1 over r10i1p1f1
        member_ids_min_length = min([len(m) for m in member_ids])
        member_id_to_keep = min([m for m in member_ids if len(m) == member_ids_min_length])
        db_cmip.append(longest_runs.filter(member_id=member_id_to_keep))

    db_cmip = scmdata.run_append(db_cmip)
    
    return db_cmip
