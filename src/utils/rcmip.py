import os.path

import pandas as pd
from openscm_units import unit_registry


def add_oscar_reported_results(derived, metric, oscar_data, overwrite_oscar_metric_name=True):
    """
    OSCAR uses a weighted ensemble so we can't calculate directly off its percentiles
    
    This function helps us make sure we get the data from the right spot
    """
    if metric in oscar_data["RCMIP name"].tolist():
        print(f"Using OSCAR reported data for {metric}")
        oscar_metric_vals = oscar_data.loc[oscar_data["RCMIP name"] == metric, :].copy()

        oscar_units = oscar_metric_vals["unit"].unique().tolist()
        derived_units = derived["unit"].unique().tolist()
            
        if len(oscar_units) > 1:
            raise AssertionError(f"OSCAR: {oscar_units}")
            
        if oscar_units != derived_units:
            print(f"Convert OSCAR units {oscar_units} to {derived_units}")
            new_vals = (
                (oscar_units["value"].values * unit_registry(oscar_units[0]))
                .to(derived_units[0])
            )
            oscar_units.loc[:, "value"] = new_vals.magnitude
            oscar_units.loc[:, "unit"] = str(new_vals.units)
        
        oscar_metric_vals = oscar_metric_vals.copy()
        oscar_metric_vals["model"] = "unspecified"

        cols_to_copy = [
            "evaluation_period_end_year",
            "evaluation_period_start_year",
            "reference_period_end_year",
            "reference_period_start_year",
            "region",
            "scenario",
            "variable", 
        ]
        if overwrite_oscar_metric_name:
            cols_to_copy.append("RCMIP name")
            
        for col_to_copy in cols_to_copy:
            try:
                copy_vals = derived[col_to_copy].dropna().unique().tolist()
            except KeyError:
                continue

            if not any([v == "Airborne Fraction|CO2" for v in copy_vals]):
                assert len(copy_vals) == 1, copy_vals

            oscar_metric_vals.loc[:, col_to_copy] = copy_vals[0]

        out = pd.concat([
            oscar_metric_vals,
            derived.loc[~derived["climate_model"].str.startswith("OSCAR"), :].copy()
        ])

    else:
        print(f"Not using OSCAR reported data for {metric}")
        out = derived

    return out


def get_monkey_patched_assessed_ranges(ars, oscar_data):
    calculate_metric_from_results_original = ars.calculate_metric_from_results

    def calculate_metric_from_results_monkey(metric, res_calc, custom_calculators=None):
        derived = calculate_metric_from_results_original(
            metric, res_calc, custom_calculators
        )
        out = add_oscar_reported_results(derived, metric, oscar_data)

        return out

    ars.calculate_metric_from_results = calculate_metric_from_results_monkey

    return ars
