import glob
import os.path
import shutil

for f in glob.glob("*.nc"):
    scen = f.split("__")[-1].split(".nc")[0]
    os.makedirs(scen, exist_ok=True)
    shutil.move(f, os.path.join(scen, f))
