The SR1.5 scenarios are taken from `data/sr15_scenarios/sr15_scenarios.csv`
in the code and data of [Nicholls et al., ERL 2020](https://doi.org/10.1088/1748-9326/ab83af), 
(see [the zenodo archive](https://doi.org/10.5281/zenodo.3726578)).


