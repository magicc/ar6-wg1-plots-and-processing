#!/bin/bash

mkdir -p data/raw/rcmip

curl -o data/raw/rcmip/rcmip-emissions-annual-means-v5-1-0.csv "https://rcmip-protocols-au.s3-ap-southeast-2.amazonaws.com/v5.1.0/rcmip-emissions-annual-means-v5-1-0.csv"
curl -o data/raw/rcmip/rcmip-concentrations-annual-means-v5-1-0.csv "https://rcmip-protocols-au.s3-ap-southeast-2.amazonaws.com/v5.1.0/rcmip-concentrations-annual-means-v5-1-0.csv"


curl -o data/raw/rcmip/rcmip-emissions-annual-means-v4-0-0.csv "https://rcmip-protocols-au.s3-ap-southeast-2.amazonaws.com/v4.0.0/rcmip-emissions-annual-means-v4-0-0.csv"
curl -o data/raw/rcmip/rcmip-concentrations-annual-means-v4-0-0.csv "https://rcmip-protocols-au.s3-ap-southeast-2.amazonaws.com/v4.0.0/rcmip-concentrations-annual-means-v4-0-0.csv"

